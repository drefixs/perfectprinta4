#!/bin/bash

#rm -rf /var/www/app/cache/*
chown --preserve-root -R root:staff /var/www/app/cache/
chmod --preserve-root -R og+wrx /var/www/app/cache/

#rm -rf /var/www/app/logs/*
chown --preserve-root -R root:staff /var/www/app/logs/
chmod --preserve-root -R og+wrx /var/www/app/logs/

chown --preserve-root -R root:staff /var/www/web/
chmod --preserve-root -R og+wrx /var/www/web/

chown --preserve-root -R root:staff /var/www/src/
chmod --preserve-root -R og+wrx /var/www/src/

chown --preserve-root -R root:staff /var/www/app/
chmod --preserve-root -R og+wrx /var/www/app/

/bin/bash -l -c "$*"
