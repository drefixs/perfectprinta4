export SYMFONY__REDIS_PORT=${REDIS_PORT_6379_TCP_PORT}
export SYMFONY__REDIS_ADDRESS=${REDIS_PORT_6379_TCP_ADDR}
export SYMFONY__DATABASE__HOST=${MYSQL_PORT_3306_TCP_ADDR}
export SYMFONY__DATABASE__ROOT__PASSWORD=${MYSQL_ENV_MYSQL_ROOT_PASSWORD}
export SYMFONY__DATABASE__USER=${MYSQL_ENV_MYSQL_USER}
export SYMFONY__DATABASE__PASSWORD=${MYSQL_ENV_MYSQL_PASSWORD}
export SYMFONY__DATABASE__NAME=${MYSQL_ENV_MYSQL_DATABASE_NAME}