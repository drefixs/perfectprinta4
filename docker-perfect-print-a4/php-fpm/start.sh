#!/bin/bash

sed -i "s@listen = /var/run/php5-fpm.sock@listen = 9000@" /etc/php5/fpm/pool.d/www.conf
sed -i "s@display_errors = Off@display_errors = On@" /etc/php5/fpm/pool.d/www.conf


echo "env[APP_SERVER_NAME] = ${APP_SERVER_NAME}" >> /etc/php5/fpm/pool.d/www.conf
echo "env[SYMFONY__REDIS_PORT] = ${REDIS_PORT_6379_TCP_PORT}" >> /etc/php5/fpm/pool.d/www.conf
echo "env[SYMFONY__REDIS_ADDRESS] = ${REDIS_PORT_6379_TCP_ADDR}" >> /etc/php5/fpm/pool.d/www.conf
echo "env[SYMFONY__DATABASE__HOST] = ${MYSQL_PORT_3306_TCP_ADDR}" >> /etc/php5/fpm/pool.d/www.conf
echo "env[SYMFONY__DATABASE__ROOT__PASSWORD] = ${MYSQL_ENV_MYSQL_ROOT_PASSWORD}" >> /etc/php5/fpm/pool.d/www.conf
echo "env[SYMFONY__DATABASE__USER] = ${MYSQL_ENV_MYSQL_USER}" >> /etc/php5/fpm/pool.d/www.conf
echo "env[SYMFONY__DATABASE__PASSWORD] = ${MYSQL_ENV_MYSQL_PASSWORD}" >> /etc/php5/fpm/pool.d/www.conf
echo "env[SYMFONY__DATABASE__NAME] = ${MYSQL_ENV_MYSQL_DATABASE_NAME}" >> /etc/php5/fpm/pool.d/www.conf

exec /usr/sbin/php5-fpm --nodaemonize



